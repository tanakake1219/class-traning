/**
 * テストのすべての教科の合計点。
 * @returns {number}
 */
function 合計() {
  return 点数.国語 + 点数.数学 + 点数.英語;
}

/**
 * テストのすべての教科の平均点。
 * @returns {number}
 */
function 平均(点数) {
  return 合計(点数) / 3;
}

/**
 * テストのすべての教科の成績。
 * @returns {string}
 */
function 成績(点数) {
  return `
国語: ${点数.国語}
数学: ${点数.数学}
英語: ${点数.英語}
合計: ${合計(点数)}
平均: ${平均(点数)}
`;
}

const 点数 = {
  国語: 87,
  数学: 70,
  英語: 68
};

console.log(成績(点数));
